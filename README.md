This repository contains the clean up script for the metadata files in IMaSC (ICFOSS Malayalam Speech Corpus).

The cleaned up metadata files corresponding to each speaker is also made available.

A graphemic pronunciation lexicon that conatins all the unique words in this corpus (In addition to many common malayalm words) is also made available

The source of entire speech corpus is here: https://huggingface.co/datasets/thennal/IMaSC
