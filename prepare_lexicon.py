#!/usr/bin/env python3

import os


# Define the user-defined function
def createlexicon(infile, outfile):
    lex = {}
    with open(infile, 'r', encoding='utf-8') as fin:
        for line in fin:
            line = line.strip()
            characters = list(line)
            characters = " ".join(['' if char == '+' else char for char in characters])
            lex[line] = characters
    with open(outfile, 'w', encoding='utf-8') as fout:
        for key in lex:
            fout.write(key + "\t" + lex[key] + "\n")


# Use the user-defined function in the main function
def main():
    # Call the user-defined function with two arguments and store the result in a variable
    infile = "words87k"
    outfile = "lexicon_g.txt"
    createlexicon(infile,outfile)

# Call the main function
if __name__ == "__main__":
    main()
